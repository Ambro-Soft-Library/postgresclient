export = DB

declare class DB {
  /**
   * The constructor of DB class.
   * @param config - configuration object, it must contain keys: 
   * user, password, host, port, database and max - for connections (default is 100).
   */
  constructor(config: object);

  /**
   * The method selects records from database.
   * @param table - Table or view name.
   * @param datas - Object that contains fields making db-params-builder library.
   * It must contains select fields, but 'where' and 'order by' statements are not essential.
   * @param inner - The inner users info for saving actions in database.
   */
  get(table: string, datas: object, inner: object): Promise

  /**
   * The function updates record in database.
   * @param table - Table or view name.
   * @param data  - key-value pairs for update. It must contain recId.
   * @param inner - The inner users info for saving actions in database.
   */
  put(table: string, data: object, inner: object): Promise

  /**
   * The function creates record in database.
   * @param table - Table or view name.
   * @param data  - key-value pairs for saving.
   * @param inner - The inner users info for saving actions in database.
   */
  post(table: string, data: object, inner: object): Promise

  /**
   * The function removes record from database.
   * @param table - Table or view name.
   * @param recId - Entry database id. 
   * @param inner - The inner users info for saving actions in database.
   */
  delete(table: string, recId: number, inner: object): Promise

  /**
   * The functin calls database function.
   * @param fn - function name.
   * @param params - parameters for function.
   */
  call(fn: string, ...params: any[]): Promise
}