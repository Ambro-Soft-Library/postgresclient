const { stringCamelCaseToSnakeCase } = require('case-translator')

module.exports  = ((array) => {
  let text = 'select'
  if (!!array) {
    if (!Array.isArray(array)) {
      array = [array]
    }
    let len = array.length
    for(let i = 0; i < len; i++) {
      text = text + (i > 0 ? ',' : '') + ' T.' + stringCamelCaseToSnakeCase(array[i])
    }
  }
  else {
    text = text + ' T.*'
  }
  return text
})
