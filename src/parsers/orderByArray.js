module.exports  = ((array) => {
  let text = ''
  if (array) {
    if (!Array.isArray(array)) {
      array = [array]
    }
    text = 'order by'
    let len = array.length
    for(let i = 0; i < len; i++) {
      text = text + (text.substring(text.length - 8) === 'order by' ? ' ' : ', ') + 'T.' + Object.keys(array[i])[0] + ' ' + Object.values(array[i])[0] 
    }
  }
  return text
})
