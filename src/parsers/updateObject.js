const whereObject = require('./whereObject')

module.exports  = ((table, datas) => {
  let setText = 'update ' + table + ' T set '
  let values = []
  let n = 0
  let wherePart = {rec_id: datas.rec_id}
  delete datas.rec_id
  let keys = Object.keys(datas)
  for (let i = 0; i < keys.length; i++) {
    setText = setText + (setText.substring(setText.length - 7) === ' T set ' ? '' : ', ') + keys[i] + ' = $' + (++n)
    values.push(datas[keys[i]])
  }
  let whereText = whereObject(wherePart, n, values)
  return {setText: setText, whereText: whereText, values: values}
})
