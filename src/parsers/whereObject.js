module.exports = (obj, n, values) => {
  let text = ''
  // თუ null, undefined ან {}-ა, მაშინ where საერთოდ არ ყოფილა
  if (!obj || Object.keys(obj).length === 0) {
    return text
  }
  // თუ არ არის ერთ გასაღებიანი ან ეს გასაღები არაა and ან or, მაშინ მთლიანად ვაქცევთ and გასაღების ქვეშ
  if (Object.keys(obj).length > 1 || Object.keys(obj)[0] !== 'and') {
    obj = { and: obj }
  }

  text += 'where '
  let components = { text, n, values }
  compoundParser('and', obj, components)
  return components.text
}

// დამხმარე ფუნქციები
function compoundParser(comp, obj, components) {
  let keys = Object.keys(obj)
  if (!obj || keys.length === 0) {
    components.text += 'true'
  } else {
    let len = keys.length
    for (let i = 0; i < len; i++) {
      components.text += i > 0 ? ' ' + comp + ' (' : '('
      let key = keys[i]
      if (key === 'and') {
        compoundParser('and', obj[key], components)
      } else if (key === 'or') {
        compoundParser('or', obj[key], components)
      } else {
        conditionParser(key, obj[key], components)
      }
      components.text += ')'
    }
  }
}

function conditionParser(key, obj, components) {
  if (typeof obj === 'object') {
    if (Array.isArray(obj)) {
      let len = obj.length
      components.text += 'T.' + key + ' in ('
      for (let i = 0; i < len; i++) {
        components.text += (i > 0 ? ', ' : '') + '$' + ++components.n
        components.values.push(obj[i])
      }
      components.text += ')'
    } else if (obj.relation === 'json') {
      Object.keys(obj.property_values).forEach((property, index) => {
        if (index) {
          components.text += ' and '
        }
        components.text += 'T.' + key + ' ->> \'' + property + '\' = $' + ++components.n
        components.values.push(obj.property_values[property])
      })
    } else if (obj.relation === 'is null' || obj.relation === 'is not null') {
      components.text += 'T.' + key + ' ' + obj.relation
    } else if (obj.relation === 'includes') {
      components.text += '$' + ++components.n + ' = any(T.' + key + ')'
      components.values.push(obj.value)
    } else {
      components.text += 'T.' + key + ' '
      switch (obj.relation) {
        case 'from':
          components.text += '>= ' + '$' + ++components.n
          break
        case 'greater':
          components.text += '> ' + '$' + ++components.n
          break
        case 'less':
          components.text += '< ' + '$' + ++components.n
          break
        case 'to':
          components.text += '<= ' + '$' + ++components.n
          break
        case 'unequal':
          components.text += '<> ' + '$' + ++components.n
          break
        case 'like':
          components.text += 'like ' + '$' + ++components.n
          obj.value = '%' + obj.value + '%'
          break
      }
      components.values.push(obj.value)
    }
  } else {
    components.text += 'T.' + key + ' = $' + ++components.n
    components.values.push(obj)
  }
}
