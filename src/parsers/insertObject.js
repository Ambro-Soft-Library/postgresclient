module.exports  = ((table, datas) => {
  let keysText = 'insert into ' + table + ' ('
  let valueText = 'values ('
  let values = []
  let n = 0
  let keys = Object.keys(datas)
  for (let i = 0; i < keys.length; i++) {
    keysText = keysText + (keysText.substring(keysText.length - 2) === ' (' ? '' : ', ') + keys[i]
    valueText = valueText + (valueText.substring(valueText.length - 2) === ' (' ? '' : ', ') + '$' + (++n)
    values.push(datas[keys[i]])
  }
  keysText = keysText + ')'
  valueText = valueText + ')'
  return {keysText: keysText, valueText: valueText, values: values}
})
