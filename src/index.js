const { Pool } = require("pg")
const { camelCaseToSnakeCase, snakeCaseToCamelCase } = require("case-translator")
const GET = require("./GET")
const POST = require("./POST")
const DELETE = require("./DELETE")
const generalSelect = require("./GET/generalSelect")
const generalDelete = require("./DELETE/generalDelete")
const generalInsertUpdate = require("./POST/generalInsertUpdate")

module.exports = class DB {
  constructor({ user, password, host, port, database, max = 100, logInfo = () => {} }) {
    global.$logInfo = logInfo
    try {
      this.pool = new Pool({ user, password, host, port, database, max })
    } catch (err) {}
  }
  getGeneralSelect() {
    return generalSelect
  }
  getGeneralDelete() {
    return generalDelete
  }
  getGeneralInsertUpdate() {
    return generalInsertUpdate
  }

  async get(table, datas = {}, inner = {}, selector) {
    let client = await this.pool.connect()
    try {
      const res = await GET(
        table,
        camelCaseToSnakeCase(datas),
        client,
        { from_db_selecter_locale: inner.fromDbSelecterLocale },
        selector
      )
      let result = []
      res.rows.forEach((elem) => result.push(snakeCaseToCamelCase(elem)))
      return result
    } finally {
    }
  }

  async post(table, datas, inner = {}, insertUpdater) {
    delete inner.fromDbSelecterLocale
    let client = await this.pool.connect()
    try {
      const res = await POST(table, camelCaseToSnakeCase(datas), client, camelCaseToSnakeCase(inner), insertUpdater)
      let result = []
      res.rows.forEach((elem) => result.push(snakeCaseToCamelCase(elem)))
      return result
    } finally {
    }
  }

  async delete(table, datas, inner = {}, deleter) {
    delete inner.fromDbSelecterLocale
    let client = await this.pool.connect()
    try {
      const res = await DELETE(table, camelCaseToSnakeCase(datas), client, camelCaseToSnakeCase(inner), deleter)
      let result = []
      res.rows.forEach((elem) => result.push(snakeCaseToCamelCase(elem)))
      return result
    } finally {
    }
  }
}
