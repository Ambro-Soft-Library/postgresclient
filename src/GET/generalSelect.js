const selectArray = require('../parsers/selectArray')
const whereObject = require('../parsers/whereObject')
const orderByArray = require('../parsers/orderByArray')
const loggerToDB = require('../../src/helpers')

module.exports = async (table, datas, client) => {
  let text
  let values = []
  // SELECT ნაწილი
  text = selectArray(datas.select)
  text = text + ' from ' + table + ' T'
  // WHERE ნაწილი
  text = text + ' ' + whereObject(datas.where, 0, values)
  // ORDER BY ნაწილი
  text = text + ' ' + orderByArray(datas.order_by)
  try {
    loggerToDB(text, values)
    const res = await client.query({ text: text, values: values })
    return res
  } finally {
  }
}
