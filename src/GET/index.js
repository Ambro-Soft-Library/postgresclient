const generalSelect = require('./generalSelect')

// რთული, მაგრამ მაინც სტანდარტული შემთხვევებისათვის ამ ცხრილს ჩაემატება tableName: accordingSelector()
const selectors = {}

// datas არის შემდეგი სტრუქტურის json-ი:
// {
//  "select": ["fieldName1", "fieldName2", ...], თუ მარტო ერთი fieldName-ია, შეიძლება კვადრატული ფრჩხილების გარეშეც დატოვება
//  "where": ეს არის json, რომელშიც აწყობილნი არიან and და or გასაღებიანი ობიექტებთა და fieldName: value ან fieldName: valueObject ან fieldName: valueArray-თი წარმოდგენილი
//    უფრო დეტალურად იხილეთ whereObject.js-ში
//  "orderBy": [{ "value1": "askDesc" }, { "value2": "askDesc" }, ...] თუ მარტო ერთი მონაცემითაა სორტირება, შეიძლება კვადრატული ფრჩხილების გარეშეც დატოვება
// }

module.exports = (async (table, datas, client, locale, selector) => {
  selector = selector || selectors[table] || generalSelect
  try {
    // Begin Transaction
    await client.query('BEGIN')
    // Transaction
    const res = await selector(table, datas, client, locale)
    // End Transaction
    await client.query('COMMIT')
    return res
  }
  catch (err) {
    // Rollback Transaction
    await client.query('ROLLBACK')
    throw err
  } finally {
    client.release()
  }
})