const MAX_LENGTH = 64
function shorter(array) {
  if (!Array.isArray(array)) {
    let str = new String(array)
    if (str.length > MAX_LENGTH) {
      str = str.substr(0, MAX_LENGTH) + '...'
    } else {
      str = array
    }
    return str
  } else {
    const rtrn = new Array(array.length)
    array.forEach((el, index) => {
      rtrn[index] = shorter(el)
    })
    return rtrn
  }
}

function loggerToDB(text, values) {
  $logInfo('to DB text, values: ' + text + ' [' + shorter(values) + ']')
}

module.exports = (text, values) => {
  return loggerToDB(text, values)
}
