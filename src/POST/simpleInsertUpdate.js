const insertObject = require('../parsers/insertObject')
const updateObject = require('../parsers/updateObject')
const loggerToDB = require('../../src/helpers')

module.exports = async (table, datas, client, inner) => {
  let text
  let values = []
  let components
  const split = table.split('.')
  let trackedQuery = split.length === 2 ?
    "select exists (SELECT * FROM information_schema.columns WHERE table_schema = '" + split[0] + "' and table_name = '" + split[1] + "' and column_name = 'in_db_changer_app') AS tracked" :
    "select exists (SELECT * FROM information_schema.columns WHERE table_name = '" + table + "' and column_name = 'in_db_changer_app') AS tracked"
  try {
    const check = await client.query(trackedQuery)
    if (!!check.rows[0].tracked) {
      Object.assign(datas, inner)
    }
    if (!datas.rec_id) {
      // INSERT შემთხვევა
      delete datas.rec_id
      components = insertObject(table, datas)
      text = components.keysText + ' ' + components.valueText + ' returning rec_id'
    } else {
      // UPDATE შემთხვევა
      const recId = datas.rec_id
      components = updateObject(table, datas)
      text = components.setText + ' ' + components.whereText + ' returning rec_id'
      // შემთხვევა, როცა მხოლოდ rec_id-ია გადმოცემული და სხვა არაფერი. ასეთი შეიძლება მოხდეს, მაგ. თუ აქვს რაიმე setsForSepareteSaving-ი
      if (components.values.length === 1) {
        return recId
      }
    }
    values.push(...components.values)
    loggerToDB(text, values)
    const { rows } = await client.query(text, values)
    return rows[0].rec_id
  } finally {
  }
}
