const generalSelect = require('../GET/generalSelect')
const simpleInsertUpdate = require('./simpleInsertUpdate')
const generalDelete = require('../DELETE/generalDelete')

module.exports = (async (table, datas, client, inner) => {
  let recId
  let setsForSeparateSaving = datas.sets_for_separate_saving
  delete datas.sets_for_separate_saving

  try {
    // ჩაწერა-ჩასწორება ძირითად ცხრილში
    recId = await simpleInsertUpdate(table, datas, client, inner)
    // ცალკე დასამუშავებელი ცხრილების მონაცემების ჩაწერა-ჩასწორება
    if (setsForSeparateSaving) {
      const { rows } = await client.query({ text: "select get_object_name_by_table('" + table + "')", rowMode: 'array' })
      let objectName = rows[0][0]
      let keys = Object.keys(setsForSeparateSaving)
      for (const key of keys) {
        const oldEntries = (await client.query({ text: "select array_agg(rec_id) from " + objectName + '_' + key + " where " + objectName + "_id = " + recId, rowMode: 'array' })).rows[0][0] || []
        const remainEntries = []
        for (const elem of setsForSeparateSaving[key]) {
          if (!!elem.rec_id) {
            remainEntries.push(elem.rec_id)
          }
          elem[objectName + '_id'] = recId
          await simpleInsertUpdate(objectName + '_' + key, elem, client, inner)
        }
        const forDeletingEntries = oldEntries.filter(elem => !remainEntries.includes(elem))
        await generalDelete(objectName + '_' + key, { rec_id: forDeletingEntries }, client, inner)
      }
    }
    const split = table.split('.')
    // შედეგ-სტრიქონის წამოღება იგივე ან, თუ არსებობს, იმის_whole ცხრილიდან    
    let wholeQuery = split.length === 2 ?
      "select exists (SELECT * FROM information_schema.tables WHERE table_schema = '" + split[0] + "' and table_name = '" + split[1] + "_whole') AS wholed" :
      "select exists (SELECT * FROM information_schema.tables WHERE table_name = '" + table + "_whole') AS wholed"
    const check = await client.query(wholeQuery)
    if (check.rows[0].wholed) {
      table += '_whole'
    }
    const res = await generalSelect(table, { where: { "rec_id": recId } }, client)
    return res
  } finally {
  }
})