const generalInsertUpdate = require('./generalInsertUpdate')

// რთული, მაგრამ მაინც სტანდარტული შემთხვევებისათვის ამ ცხრილს ჩაემატება tableName: accordingInsertUpdater()
const insertUpdates = {}

module.exports = (async (table, datas, client, inner, insertUpdater) => {
  insertUpdater = insertUpdater || insertUpdates[table] || generalInsertUpdate
  
  try {
    // Begin Transaction
    await client.query('BEGIN')
    // Transaction
    const res = await insertUpdater(table, datas, client, inner)
    // End Transaction
    await client.query('COMMIT')
    return res
  }
  catch(err) { 
    // Rollback Transaction
    await client.query('ROLLBACK')
    throw err
  } finally {
    client.release()
  }
})