const loggerToDB = require('../../src/helpers')

module.exports = async (table, datas, client, inner) => {
  let text = 'delete from ' + table + ' where rec_id = $1 returning rec_id'
  let values = [datas.rec_id]
  try {
    loggerToDB(text, values)
    let res = await client.query(text, values)
    const split = table.split('.')
    let trackedQuery = split.length === 2 ?
      "select exists (SELECT * FROM information_schema.columns WHERE table_schema = '" + split[0] + "' and table_name = '" + split[1] + "' and column_name = 'in_db_changer_app') AS tracked" :
      "select exists (SELECT * FROM information_schema.columns WHERE table_name = '" + table + "' and column_name = 'in_db_changer_app') AS tracked"
    const check = await client.query(trackedQuery)
    if (!!check.rows[0].tracked && res.rows[0]) {
      text = split.length === 2 ?
        'insert into ' + split[0] + '_' + split[1] + '_in_db_changes (rec_id, in_db_changer_app, in_db_changer_ip, in_db_changer_rec_id) values ($1, $2, $3, $4)' :
        'insert into ' + table + '_in_db_changes (rec_id, in_db_changer_app, in_db_changer_ip, in_db_changer_rec_id) values ($1, $2, $3, $4)'
      values = [res.rows[0].rec_id]
      values.push(inner.in_db_changer_app)
      values.push(inner.in_db_changer_ip)
      values.push(inner.in_db_changer_rec_id)
      loggerToDB(text, values)
      await client.query(text, values)
    }
    return res
  } finally {
  }
}
