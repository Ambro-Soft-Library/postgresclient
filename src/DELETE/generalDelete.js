const simpleDelete = require('./simpleDelete')

module.exports  = (async (table, datas, client, inner) => {
  try {
    let res = {rows: []}
    let recIds = datas.rec_id
    if (typeof recIds !== 'object') {
      recIds = []
      recIds.push(datas.rec_id)
    }
    for (i = 0; i < recIds.length; i++) {
      let simpleRes = await simpleDelete(table, {rec_id: recIds[i]}, client, inner)  
      if (simpleRes.rows && simpleRes.rows[0])
        res.rows.push({rec_id: simpleRes.rows[0].rec_id})
    }
    return res
  } finally { }
})