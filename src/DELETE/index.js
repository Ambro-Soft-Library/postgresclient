const generalDelete = require('./generalDelete')

// რთული, მაგრამ მაინც სტანდარტული შემთხვევებისათვის ამ ცხრილს ჩაემატება tableName: accordingDeleter()
const deletes = {}

module.exports = (async (table, datas, client, inner, deleter) => {
  deleter = deleter || deletes[table] || generalDelete
  
  try {
    // Begin Transaction
    await client.query('BEGIN')
    // Transaction
    const res = await deleter(table, datas, client, inner)
    // End Transaction
    await client.query('COMMIT')
    return res
  }
  catch (err) {
    // Rollback Transaction
    await client.query('ROLLBACK')
    throw err
  } finally {
    client.release()
  }
})