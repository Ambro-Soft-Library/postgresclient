import test from 'ava'
import DB from '../src'

const table_name = 'xxx_test_for_saving_changes_by_trigger'
const config = {
      user: 'dtm',
      password: 'Dat0Tok@Murman1',
      host: 'ambro.ge',
      port: '5432',
      database: 'deservice',
    }
const db = new DB(config)

let inner = {inDbChangerApp : 'deService', inDbChangerIp: '201.202.203.204', inDbChangerRecId: 17, fromDbSelecterLocale: 'ka'}
let recId = 16.3 //  = 0 - insert; > 0 - update and select; < 0 - delete; .2 - select log

if (recId === 0) {
  test('post(insert_) method', async t => {
    const datas = { name: 'მურმანი', age: 57 }
    try {
      let res = await db.post(table_name, datas, inner)
      recId = res[0].recId
      console.log('inserted recId = ', recId, '. For next testing set this positive value to update and select, next negative value to delete')
      t.is(res[0].name, 'მურმანი')
    }
    catch(err) {console.log('error:', err)}
  })
} else if (recId % 1 !== 0) {
  test('get method', async t => {
    const datas = {
      where: { recId: Math.floor(Math.abs(recId)) }
    }
    await db.get(table_name + '_in_db_changes', datas, inner)
      .then(res => {
        console.log('log (always 4 records):\n', JSON.stringify(res[0]), '\n', JSON.stringify(res[1]), '\n', JSON.stringify(res[2]), '\n', JSON.stringify(res[3]))
        t.is(Array.isArray(res), true)
      })
      .catch(console.log)
  })
} else if (recId > 0) {
  test('post(_update) method', async t => {
    const datas = { recId: recId, name: 'ამბროლა', age: 56 }
    await db.post(table_name, datas, inner)
      .then(res => {
        t.is(res[0].name, 'ამბროლა')
      })
      .catch(console.log)
  })
  test('get method', async t => {
    const datas = {
      "select": ["recId", "name"],
      "where": { "name": ["მურმანი", "ამბროლა"] },
      "orderBy": { "age": "desc" }
    }
    await db.get(table_name, datas, inner)
      .then(res => {
        console.log('log (always 4 records):\n', JSON.stringify(res[0]), '\n', JSON.stringify(res[1]), '\n', JSON.stringify(res[2]), '\n', JSON.stringify(res[3]))
        t.is(Array.isArray(res), true)
      })
      .catch(console.log)
  }) 
} else {
  test('delete method', async t => {
    const datas = { recId: - recId }
    await db.delete(table_name, datas, inner)
      .then(res => {
        t.is(res[0].recId, - recId)
      })
      .catch(console.log)
  })
}

